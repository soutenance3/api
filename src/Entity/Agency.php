<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\AgencyDailyController;
use App\Repository\AgencyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: AgencyRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get", "post",
        "statistics" => [
            "method" => "GET",
            "path" => "/agencies/statistics/daily",
            "controller" => AgencyDailyController::class,
            "pagination_enabled" => false,
            "openapi_context" => [
                "summary" => "Renvoi les données statistiques journalière des 7 derniers jours par agence",
                "requestBody" => [],
                "responses" => [
                    "200" => [
                        "description" => "OK",
                        "content" => [
                            "application/ld+json" => [
                                "schema" => [
                                    "type" => "object",
                                    "nullable" => true,
                                    "properties" => [
                                        "day" => [
                                            "type" => "string",
                                            "description" => "Jour correspondant à la date"
                                        ],
                                        "date" => [
                                            "type" => "integer",
                                            "description" => "Jour de la date en chiffre"
                                        ],
                                        "data" => [
                                            "type" => "object",
                                            "nullable" => true,
                                            "properties" => [
                                                "name" => [
                                                    "type" => "string",
                                                    "description" => "Nom de l'entreprise"
                                                ],
                                                "amount" => [
                                                    "type" => "string",
                                                    "description" => "Valeur des ventes"
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
        ]

    ]
)]
#[ORM\UniqueConstraint(fields: ['name', 'company'])]
class Agency
{
    use SoftDeleteableEntity;
    use BlameableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $code;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    private $address;

    #[ORM\ManyToOne(targetEntity: Company::class, inversedBy: 'agencies')]
    #[ORM\JoinColumn(nullable: false)]
    private $company;

    #[ORM\OneToMany(mappedBy: 'agency', targetEntity: Sale::class, orphanRemoval: true)]
    private $sales;

    #[ORM\OneToMany(mappedBy: 'agency', targetEntity: ProviderOrder::class, orphanRemoval: true)]
    private $providerOrders;

    #[ORM\OneToMany(mappedBy: 'agency', targetEntity: CustomerOrder::class, orphanRemoval: true)]
    private $customerOrders;

    public function __construct()
    {
        $this->sales = new ArrayCollection();
        $this->providerOrders = new ArrayCollection();
        $this->customerOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection<int, Sale>
     */
    public function getSales(): Collection
    {
        return $this->sales;
    }

    public function addSale(Sale $sale): self
    {
        if (!$this->sales->contains($sale)) {
            $this->sales[] = $sale;
            $sale->setAgency($this);
        }

        return $this;
    }

    public function removeSale(Sale $sale): self
    {
        if ($this->sales->removeElement($sale)) {
            // set the owning side to null (unless already changed)
            if ($sale->getAgency() === $this) {
                $sale->setAgency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProviderOrder>
     */
    public function getProviderOrders(): Collection
    {
        return $this->providerOrders;
    }

    public function addProviderOrder(ProviderOrder $providerOrder): self
    {
        if (!$this->providerOrders->contains($providerOrder)) {
            $this->providerOrders[] = $providerOrder;
            $providerOrder->setAgency($this);
        }

        return $this;
    }

    public function removeProviderOrder(ProviderOrder $providerOrder): self
    {
        if ($this->providerOrders->removeElement($providerOrder)) {
            // set the owning side to null (unless already changed)
            if ($providerOrder->getAgency() === $this) {
                $providerOrder->setAgency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CustomerOrder>
     */
    public function getCustomerOrders(): Collection
    {
        return $this->customerOrders;
    }

    public function addCustomerOrder(CustomerOrder $customerOrder): self
    {
        if (!$this->customerOrders->contains($customerOrder)) {
            $this->customerOrders[] = $customerOrder;
            $customerOrder->setAgency($this);
        }

        return $this;
    }

    public function removeCustomerOrder(CustomerOrder $customerOrder): self
    {
        if ($this->customerOrders->removeElement($customerOrder)) {
            // set the owning side to null (unless already changed)
            if ($customerOrder->getAgency() === $this) {
                $customerOrder->setAgency(null);
            }
        }

        return $this;
    }
}
