<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CompanyStatisticsController;
use App\Controller\JoinCompanyController;
use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ApiResource(
    collectionOperations: [
        "get","post",
        "join" => [
            "method" => "PATCH",
            "path" => "/companies/join/{token}",
            "controller" => JoinCompanyController::class,
            "openapi_context" => [
                "summary" => "Permet de rejoindre une entreprise",
                "requestBody" => [
                    "content" => []
                ],
                "responses" => [
                    "200" => [
                        "description" =>'OK',
                        "content" => [
                            "application/ld+json" => [
                                "schema" => [
                                    'type' => 'object',
                                    'properties' => [
                                        'company' => [
                                            'type' => 'string($iri-reference)',
                                            'example' => '/api/companies/1'
                                        ]
                                    ],
                                ]
                            ],
                            "application/json" => [
                                "schema" => [
                                    'type' => 'object',
                                    'properties' => [
                                        'company' => [
                                            'type' => 'string($iri-reference)',
                                            'example' => '/api/companies/1'
                                        ]
                                    ],
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        "statistics" => [
            "method" => "GET",
            "path" => "/companies/statistics",
            "controller" => CompanyStatisticsController::class,
            "pagination_enabled" => false,
            "openapi_context" => [
                "summary" => "Renvoit les données statistiques sur l'entreprise",
                "requestBody" => [],
                "responses" => [
                    "200" => [
                        "description" => "OK",
                        "content" => [
                            "application/ld+json" => [
                                "schema" => [
                                    "type" => "object",
                                    "nullable" => true,
                                    "properties" => [
                                        "users" => [
                                            "type" => "integer",
                                        ],
                                        "agencies" => [
                                            "type" => "integer",
                                        ],
                                        "customers" => [
                                            "type" => "integer",
                                        ],
                                        "providers" => [
                                            "type" => "integer",
                                        ],
                                        "categories" => [
                                            "type" => "integer",
                                        ],
                                        "articles" => [
                                            "type" => "integer",
                                        ],
                                        "sales" => [
                                            "type" => "integer",
                                        ],

                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
        ]
    ]
)]
#[ORM\Table(name: "companies")]
#[ORM\Entity(repositoryClass: CompanyRepository::class)]
class Company
{
    use SoftDeleteableEntity;
    use BlameableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $email;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $ifu;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $rccm;

    #[ORM\Column(type: 'text', nullable: true)]
    private $description;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $address;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $phone;

    #[ORM\OneToMany(mappedBy: 'company', targetEntity: User::class)]
    private $users;

    #[ORM\OneToMany(mappedBy: 'company', targetEntity: Agency::class, orphanRemoval: true)]
    private $agencies;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $logo;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $token;

    #[ORM\OneToMany(mappedBy: 'company', targetEntity: Category::class, orphanRemoval: true)]
    private $categories;

    #[ORM\OneToMany(mappedBy: 'company', targetEntity: Article::class)]
    private $articles;

    #[ORM\OneToMany(mappedBy: 'company', targetEntity: Provider::class, orphanRemoval: true)]
    private $providers;

    #[ORM\OneToMany(mappedBy: 'company', targetEntity: Customer::class, orphanRemoval: true)]
    private $customers;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->agencies = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->providers = new ArrayCollection();
        $this->customers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIfu(): ?string
    {
        return $this->ifu;
    }

    public function setIfu(?string $ifu): self
    {
        $this->ifu = $ifu;

        return $this;
    }

    public function getRccm(): ?string
    {
        return $this->rccm;
    }

    public function setRccm(?string $rccm): self
    {
        $this->rccm = $rccm;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCompany($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCompany() === $this) {
                $user->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Agency>
     */
    public function getAgencies(): Collection
    {
        return $this->agencies;
    }

    public function addAgency(Agency $agency): self
    {
        if (!$this->agencies->contains($agency)) {
            $this->agencies[] = $agency;
            $agency->setCompany($this);
        }

        return $this;
    }

    public function removeAgency(Agency $agency): self
    {
        if ($this->agencies->removeElement($agency)) {
            // set the owning side to null (unless already changed)
            if ($agency->getCompany() === $this) {
                $agency->setCompany(null);
            }
        }

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return Collection<int, Category>
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->setCompany($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getCompany() === $this) {
                $category->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Article>
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setCompany($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getCompany() === $this) {
                $article->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Provider>
     */
    public function getProviders(): Collection
    {
        return $this->providers;
    }

    public function addProvider(Provider $provider): self
    {
        if (!$this->providers->contains($provider)) {
            $this->providers[] = $provider;
            $provider->setCompany($this);
        }

        return $this;
    }

    public function removeProvider(Provider $provider): self
    {
        if ($this->providers->removeElement($provider)) {
            // set the owning side to null (unless already changed)
            if ($provider->getCompany() === $this) {
                $provider->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Customer>
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    public function addCustomer(Customer $customer): self
    {
        if (!$this->customers->contains($customer)) {
            $this->customers[] = $customer;
            $customer->setCompany($this);
        }

        return $this;
    }

    public function removeCustomer(Customer $customer): self
    {
        if ($this->customers->removeElement($customer)) {
            // set the owning side to null (unless already changed)
            if ($customer->getCompany() === $this) {
                $customer->setCompany(null);
            }
        }

        return $this;
    }
}
