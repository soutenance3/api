<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
#[ApiResource]
#[ORM\UniqueConstraint(fields: ['designation', 'company'])]
class Article
{
    use SoftDeleteableEntity;
    use BlameableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $code;

    #[ORM\Column(type: 'string', length: 255)]
    private $designation;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $image;

    #[ORM\Column(type: 'float')]
    private $price;

    #[ORM\Column(type: 'float')]
    private $tva;

    #[ORM\Column(type: 'integer')]
    private $warning;

    #[ORM\Column(type: 'integer')]
    private $critique;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'articles')]
    #[ORM\JoinColumn(nullable: false)]
    private $category;

    #[ORM\ManyToOne(targetEntity: Company::class, inversedBy: 'articles')]
    private $company;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: StockMovement::class, orphanRemoval: true)]
    private $stockMovements;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: SalesLine::class, orphanRemoval: true)]
    private $salesLines;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: ProviderOrdersLine::class, orphanRemoval: true)]
    private $providerOrdersLines;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: CustomerOrdersLine::class, orphanRemoval: true)]
    private $customerOrdersLines;

    public function __construct()
    {
        $this->stockMovements = new ArrayCollection();
        $this->salesLines = new ArrayCollection();
        $this->providerOrdersLines = new ArrayCollection();
        $this->customerOrdersLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getTva(): ?float
    {
        return $this->tva;
    }

    public function setTva(float $tva): self
    {
        $this->tva = $tva;

        return $this;
    }

    public function getWarning(): ?int
    {
        return $this->warning;
    }

    public function setWarning(int $warning): self
    {
        $this->warning = $warning;

        return $this;
    }

    public function getCritique(): ?int
    {
        return $this->critique;
    }

    public function setCritique(int $critique): self
    {
        $this->critique = $critique;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection<int, StockMovement>
     */
    public function getStockMovements(): Collection
    {
        return $this->stockMovements;
    }

    public function addStockMovement(StockMovement $stockMovement): self
    {
        if (!$this->stockMovements->contains($stockMovement)) {
            $this->stockMovements[] = $stockMovement;
            $stockMovement->setArticle($this);
        }

        return $this;
    }

    public function removeStockMovement(StockMovement $stockMovement): self
    {
        if ($this->stockMovements->removeElement($stockMovement)) {
            // set the owning side to null (unless already changed)
            if ($stockMovement->getArticle() === $this) {
                $stockMovement->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SalesLine>
     */
    public function getSalesLines(): Collection
    {
        return $this->salesLines;
    }

    public function addSalesLine(SalesLine $salesLine): self
    {
        if (!$this->salesLines->contains($salesLine)) {
            $this->salesLines[] = $salesLine;
            $salesLine->setArticle($this);
        }

        return $this;
    }

    public function removeSalesLine(SalesLine $salesLine): self
    {
        if ($this->salesLines->removeElement($salesLine)) {
            // set the owning side to null (unless already changed)
            if ($salesLine->getArticle() === $this) {
                $salesLine->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProviderOrdersLine>
     */
    public function getProviderOrdersLines(): Collection
    {
        return $this->providerOrdersLines;
    }

    public function addProviderOrdersLine(ProviderOrdersLine $providerOrdersLine): self
    {
        if (!$this->providerOrdersLines->contains($providerOrdersLine)) {
            $this->providerOrdersLines[] = $providerOrdersLine;
            $providerOrdersLine->setArticle($this);
        }

        return $this;
    }

    public function removeProviderOrdersLine(ProviderOrdersLine $providerOrdersLine): self
    {
        if ($this->providerOrdersLines->removeElement($providerOrdersLine)) {
            // set the owning side to null (unless already changed)
            if ($providerOrdersLine->getArticle() === $this) {
                $providerOrdersLine->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CustomerOrdersLine>
     */
    public function getCustomerOrdersLines(): Collection
    {
        return $this->customerOrdersLines;
    }

    public function addCustomerOrdersLine(CustomerOrdersLine $customerOrdersLine): self
    {
        if (!$this->customerOrdersLines->contains($customerOrdersLine)) {
            $this->customerOrdersLines[] = $customerOrdersLine;
            $customerOrdersLine->setArticle($this);
        }

        return $this;
    }

    public function removeCustomerOrdersLine(CustomerOrdersLine $customerOrdersLine): self
    {
        if ($this->customerOrdersLines->removeElement($customerOrdersLine)) {
            // set the owning side to null (unless already changed)
            if ($customerOrdersLine->getArticle() === $this) {
                $customerOrdersLine->setArticle(null);
            }
        }

        return $this;
    }
}
