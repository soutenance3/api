<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [
            "security" => "is_granted('ROLE_ADMIN') or is_granted('ROLE_OWNER')",
            "security_message" => "Seul l'administrateur peut recupérer la liste des utilisateurs",
        ], "post"
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('ROLE_USER') and object == user",
            "security_message" => "Impossible de récupérer les informations d'un autres utilisateurs",
        ], "patch", "put",
        "delete" => [
            "security" => "is_granted('ROLE_USER') and object == user",
            "security_message" => "Impossible de supprimer les informations d'un autres utilisateurs",
        ]
    ],
    denormalizationContext: [
        'groups' => ['user:write']
    ],
    normalizationContext: [
        'groups' => ['user:read']
    ]
)]
#[ORM\Table(name: "users")]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['user:read', 'user:write'])]
    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $email;

    #[Groups(['user:read'])]
    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[Groups(['user:read', 'user:write'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[Groups(['user:read', 'user:write'])]
    #[ORM\Column(type: 'date', nullable: true)]
    private $birthday;

    #[Groups(['user:read', 'user:write'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $address;

    #[Groups(['user:read', 'user:write'])]
    #[ORM\ManyToOne(targetEntity: Company::class, inversedBy: 'users')]
    private $company;

    #[Groups(['user:read', 'user:write'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $photo;

    #[Groups(['user:write'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $plainPassword;

    #[ORM\Column(type: 'integer')]
    private $token;

    #[Groups(['user:read', 'user:write'])]
    #[ORM\Column(type: 'boolean', options: ["default" => false])]
    private $isVerified;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
         $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    #[Groups(['user:read', 'user:write'])]
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    #[Groups(['user:read', 'user:write'])]
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getToken(): ?int
    {
        return $this->token;
    }

    public function setToken(int $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }
}
