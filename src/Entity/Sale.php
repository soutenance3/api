<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SaleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: SaleRepository::class)]
#[ApiResource]
class Sale
{
    use SoftDeleteableEntity;
    use BlameableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $code;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\Column(type: 'text', nullable: true)]
    private $comment;

    #[ORM\ManyToOne(targetEntity: Agency::class, inversedBy: 'sales')]
    #[ORM\JoinColumn(nullable: false)]
    private $agency;

    #[ORM\OneToMany(mappedBy: 'sale', targetEntity: SalesLine::class, orphanRemoval: true)]
    private $salesLines;

    public function __construct()
    {
        $this->salesLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAgency(): ?Agency
    {
        return $this->agency;
    }

    public function setAgency(?Agency $agency): self
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * @return Collection<int, SalesLine>
     */
    public function getSalesLines(): Collection
    {
        return $this->salesLines;
    }

    public function addSalesLine(SalesLine $salesLine): self
    {
        if (!$this->salesLines->contains($salesLine)) {
            $this->salesLines[] = $salesLine;
            $salesLine->setSale($this);
        }

        return $this;
    }

    public function removeSalesLine(SalesLine $salesLine): self
    {
        if ($this->salesLines->removeElement($salesLine)) {
            // set the owning side to null (unless already changed)
            if ($salesLine->getSale() === $this) {
                $salesLine->setSale(null);
            }
        }

        return $this;
    }
}
