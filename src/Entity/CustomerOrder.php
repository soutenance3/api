<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CustomerOrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: CustomerOrderRepository::class)]
#[ApiResource]
class CustomerOrder
{
    use SoftDeleteableEntity;
    use BlameableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $code;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\Column(type: 'text', nullable: true)]
    private $comment;

    #[ORM\ManyToOne(targetEntity: Agency::class, inversedBy: 'customerOrders')]
    #[ORM\JoinColumn(nullable: false)]
    private $agency;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'customerOrders')]
    #[ORM\JoinColumn(nullable: false)]
    private $customer;

    #[ORM\OneToMany(mappedBy: 'customerOrder', targetEntity: CustomerOrdersLine::class, orphanRemoval: true)]
    private $customerOrdersLines;

    public function __construct()
    {
        $this->customerOrdersLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAgency(): ?Agency
    {
        return $this->agency;
    }

    public function setAgency(?Agency $agency): self
    {
        $this->agency = $agency;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection<int, CustomerOrdersLine>
     */
    public function getCustomerOrdersLines(): Collection
    {
        return $this->customerOrdersLines;
    }

    public function addCustomerOrdersLine(CustomerOrdersLine $customerOrdersLine): self
    {
        if (!$this->customerOrdersLines->contains($customerOrdersLine)) {
            $this->customerOrdersLines[] = $customerOrdersLine;
            $customerOrdersLine->setCustomerOrder($this);
        }

        return $this;
    }

    public function removeCustomerOrdersLine(CustomerOrdersLine $customerOrdersLine): self
    {
        if ($this->customerOrdersLines->removeElement($customerOrdersLine)) {
            // set the owning side to null (unless already changed)
            if ($customerOrdersLine->getCustomerOrder() === $this) {
                $customerOrdersLine->setCustomerOrder(null);
            }
        }

        return $this;
    }
}
