<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProviderOrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: ProviderOrderRepository::class)]
#[ApiResource]
class ProviderOrder
{
    use SoftDeleteableEntity;
    use BlameableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $code;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\Column(type: 'text', nullable: true)]
    private $comment;

    #[ORM\ManyToOne(targetEntity: Provider::class, inversedBy: 'providerOrders')]
    #[ORM\JoinColumn(nullable: false)]
    private $provider;

    #[ORM\ManyToOne(targetEntity: Agency::class, inversedBy: 'providerOrders')]
    #[ORM\JoinColumn(nullable: false)]
    private $agency;

    #[ORM\OneToMany(mappedBy: 'providerOrder', targetEntity: ProviderOrdersLine::class, orphanRemoval: true)]
    private $providerOrdersLines;

    public function __construct()
    {
        $this->providerOrdersLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setProvider(?Provider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getAgency(): ?Agency
    {
        return $this->agency;
    }

    public function setAgency(?Agency $agency): self
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * @return Collection<int, ProviderOrdersLine>
     */
    public function getProviderOrdersLines(): Collection
    {
        return $this->providerOrdersLines;
    }

    public function addProviderOrdersLine(ProviderOrdersLine $providerOrdersLine): self
    {
        if (!$this->providerOrdersLines->contains($providerOrdersLine)) {
            $this->providerOrdersLines[] = $providerOrdersLine;
            $providerOrdersLine->setProviderOrder($this);
        }

        return $this;
    }

    public function removeProviderOrdersLine(ProviderOrdersLine $providerOrdersLine): self
    {
        if ($this->providerOrdersLines->removeElement($providerOrdersLine)) {
            // set the owning side to null (unless already changed)
            if ($providerOrdersLine->getProviderOrder() === $this) {
                $providerOrdersLine->setProviderOrder(null);
            }
        }

        return $this;
    }
}
