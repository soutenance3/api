<?php

namespace App\Controller\Admin;

use App\Entity\ProviderOrdersLine;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProviderOrdersLineCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProviderOrdersLine::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
