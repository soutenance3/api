<?php

namespace App\Controller\Admin;

use App\Entity\StockMovement;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class StockMovementCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return StockMovement::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
