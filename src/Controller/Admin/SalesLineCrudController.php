<?php

namespace App\Controller\Admin;

use App\Entity\SalesLine;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SalesLineCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SalesLine::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
