<?php

namespace App\Controller\Admin;

use App\Entity\Agency;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Company;
use App\Entity\Customer;
use App\Entity\Provider;
use App\Entity\ProviderOrder;
use App\Entity\Sale;
use App\Entity\StockMovement;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Api');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Tableau de bord', 'fa fa-home');
        yield MenuItem::subMenu("Personnes", "fa fa-users")->setSubItems([
            MenuItem::linkToCrud('Utilisateurs', 'fa fa-users', User::class),
            MenuItem::linkToCrud('Clients', 'fa fa-users', Customer::class),
            MenuItem::linkToCrud('Fournisseur', 'fa fa-users', Provider::class),
        ]);
        yield MenuItem::subMenu("Entreprises", "fa fa-building")->setSubItems([
            MenuItem::linkToCrud('Entreprises', 'fa fa-building', Company::class),
            MenuItem::linkToCrud('Agences', 'fa fa-map-location', Agency::class)
        ]);
        yield MenuItem::subMenu("Produit", "")->setSubItems([
            MenuItem::linkToCrud('Catégories', 'fa ', Category::class),
            MenuItem::linkToCrud('Articles', 'fa fa-article', Article::class),
        ]);
        yield MenuItem::subMenu("Opération", "")->setSubItems([
            MenuItem::linkToCrud('Mouvement stock', 'fa fa-', StockMovement::class),
            MenuItem::linkToCrud('Ventes', 'fa fa-', Sale::class),
            MenuItem::linkToCrud('Commandes', 'fa fa-', ProviderOrder::class),
        ]);
    }

    public function configureCrud(): Crud
    {
        return Crud::new()
            ->setEntityPermission('ROLE_ADMIN')
            ->setPageTitle("index", "Liste des %entity_label_plural% | FireFly")
            ->setPageTitle("new", "Ajouter %entity_label_singular% | FireFly")
            ->setPageTitle("detail", "Détails | FireFly")
            ->setPageTitle("edit", "Modifier %entity_label_singular% | FireFly")
            ->setPaginatorPageSize(10);
    }

    public function configureActions(): Actions
    {
        $actions = parent::configureActions();
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action->setIcon('fa fa-plus')->setLabel('Ajouter');
            })
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setLabel('Voir');
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setLabel('Modifier');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setLabel('Supprimer');
            })
            ->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-edit')->setLabel('Modifier');
            })
            ->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {
                return $action->setIcon('fa fa-trash')->setLabel('Supprimer');
            })
            ->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {
                return $action->setIcon('fa fa-arrow-left')->setLabel('Revenir à la liste');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action->setIcon('fa fa-save')->setLabel('Enregistrer');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action->setIcon('fa fa-plus')->setLabel('Enregistrer puis Ajouter');
            })
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action->setLabel('Enregistrer');
            })
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE, function (Action $action) {
                return $action->setLabel('Enregistrer et Continuer');
            })
            ;
    }
}
