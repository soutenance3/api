<?php

namespace App\Controller\Admin;

use App\Entity\CustomerOrdersLine;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CustomerOrdersLineCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CustomerOrdersLine::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
