<?php

namespace App\Controller\Admin;

use App\Entity\ProviderOrder;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProviderOrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProviderOrder::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
