<?php

namespace App\Controller;

use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class CompanyStatisticsController extends AbstractController
{
    public function __invoke(UserService $userService) : JsonResponse|null
    {
        $user = $userService->getCurrentUser();
        if (null == $user || null == $user->getCompany() || !in_array("ROLE_OWNER", $user->getRoles())){
            return null;
        }
        $company = $user->getCompany();

        $sales = 0;
        foreach ($company->getAgencies() as $agency){
            $sales += count($agency->getSales());
        }

        return new JsonResponse([
            "users" => count($company->getUsers()),
            "agencies" => count($company->getAgencies()),
            "customers" => count($company->getCustomers()),
            "providers" => count($company->getProviders()),
            "categories" => count($company->getCategories()),
            "articles" => count($company->getArticles()),
            "sales" => $sales
        ]);
    }
}