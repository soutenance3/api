<?php
    namespace App\Controller;

    use App\Repository\AgencyRepository;
    use App\Service\UserService;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\JsonResponse;

    class AgencyDailyController extends AbstractController
    {
        public function __invoke(UserService $userService, AgencyRepository $agencyRepository): null|JsonResponse
        {
            $user = $userService->getCurrentUser();
            if (null == $user || null == $user->getCompany() || !in_array("ROLE_OWNER", $user->getRoles())){
                return null;
            }
            $rep = [];
            $company = $user->getCompany();

            $dayStringEquiv = [
                "Mon" => "Lundi",
                "Tue" => "Mardi",
                "Wed" => "Mercredi",
                "Thu" => "Jeudi",
                "Fri" => "Vendredi",
                "Sat" => "Samedi",
                "Sun" => "Dimanche"
            ];

            for ($i = -6; $i <= 0; $i++){
                $date = new \DateTime("$i day");
                $data = [
                    "day" => $dayStringEquiv[$date->format("D")],
                    "date" => $date->format("d"),
                    "data" => $agencyRepository->findStatsDaily($company->getId())
                ];

                $rep[] = $data;
            }
            return new JsonResponse($rep);
        }
    }