<?php

namespace App\Controller;

use App\Repository\CompanyRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;

class JoinCompanyController extends AbstractController
{

    public function __invoke(String $token, CompanyRepository $companyRepository, UserRepository $userRepository, Security $security, EntityManagerInterface $entityManager): JsonResponse
    {
        $company = $companyRepository->findOneBy(['token' => $token]);
        $id = 0;
        if ($company){
            $id = $company->getId();
            $user = $userRepository->findOneBy(['id' => $security->getUser()->getId()]);
            $user->setRoles([]);
            $company->addUser($user);
            $entityManager->flush();
        }

        return new JsonResponse([
            'company' => "/api/companies/". $id
        ]);
    }
}
