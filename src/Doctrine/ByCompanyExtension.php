<?php

namespace App\Doctrine;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Agency;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Company;
use App\Entity\Customer;
use App\Entity\CustomerOrder;
use App\Entity\CustomerOrdersLine;
use App\Entity\Provider;
use App\Entity\ProviderOrder;
use App\Entity\ProviderOrdersLine;
use App\Entity\Sale;
use App\Entity\SalesLine;
use App\Entity\StockMovement;
use App\Entity\User;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;

/**
 * Ajoute une condition dans la requête pour ne renvoyer que les éléments concernés par l'utilisateur
 * c'est-à-dire uniquement les éléments de sa compagnie
 */
class ByCompanyExtension implements QueryCollectionExtensionInterface
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        if ($resourceClass == Company::class || !$this->security->isGranted("ROLE_USER")|| null === $user = $this->security->getUser()) {
            return;
        }

        // Liste des classes concernées par le filtre sur la compagnie
        $withoutJoin = [
            Agency::class,
            Article::class,
            Category::class,
            Customer::class,
            Provider::class,
            User::class
        ];
        $directJoinOnAgency = [
            Sale::class,
            ProviderOrder::class,
            CustomerOrder::class
        ];
        $directJoinOnArticle = [
            SalesLine::class,
            ProviderOrdersLine::class,
            CustomerOrdersLine::class,
            StockMovement::class
        ];

        $rootAlias = $queryBuilder->getRootAliases()[0];
        if (in_array($resourceClass, $withoutJoin)){
            $queryBuilder->andWhere(sprintf('%s.company = :company', $rootAlias));
        }
        if (in_array($resourceClass, $directJoinOnAgency)){
            $queryBuilder->innerJoin(Agency::class, "agency", 'WITH', sprintf("%s.agency = agency.id", $rootAlias));
            $queryBuilder->andWhere('agency.company = :company');
        }
        if (in_array($resourceClass, $directJoinOnArticle)){
            $queryBuilder->join(Article::class, "a", conditionType: 'WITH', condition: sprintf("%s.article = a.id", $rootAlias));
            $queryBuilder->andWhere('a.company = :company');
        }

        $queryBuilder->setParameter('company', $user->getCompany());
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }
}