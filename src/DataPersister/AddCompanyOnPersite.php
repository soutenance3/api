<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Customer;
use App\Entity\Provider;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Ajoute automatiquement la compagnie dans l'entité lors de l'enregistrement
 */
class AddCompanyOnPersite implements ContextAwareDataPersisterInterface
{
    private EntityManagerInterface $_em;
    private UserInterface|null $_user;
    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->_em = $em;
        $this->_user = $security->getUser();
    }

    public function supports($data, array $context = []): bool
    {
        $classNames = [
            Article::class,
            Category::class,
            Customer::class,
            Provider::class
        ];
        return in_array(get_class($data), $classNames);
    }

    public function persist($data, array $context = [])
    {
        $data->setCompany($this->_user->getCompany());
        $this->_em->persist($data);
        $this->_em->flush();
    }

    public function remove($data, array $context = [])
    {
        $this->_em->remove($data);
        $this->_em->flush();
    }
}