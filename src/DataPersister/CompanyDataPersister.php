<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Company;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class CompanyDataPersister implements ContextAwareDataPersisterInterface
{
    private EntityManagerInterface $_em;
    private UserInterface|null $_user;
    public function __construct(EntityManagerInterface $em, Security $security, UserRepository $userRepository)
    {
        $this->_em = $em;
        if ($security->getUser()){
            $this->_user = $userRepository->findOneBy(['id' => $security->getUser()->getId()]);
        } else $this->_user = null;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Company;
    }

    public function persist($data, array $context = [])
    {
        $roles = $this->_user->getRoles();
        $roles[] = "ROLE_OWNER";
        $this->_user->setRoles($roles);

        $data->addUser($this->_user);
        $data->setToken(md5($data->getName()));

        $this->_em->persist($data);
        $this->_em->flush();
    }

    public function remove($data, array $context = [])
    {
        $this->_em->remove($data);
        $this->_em->flush();
    }
}