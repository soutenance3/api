<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Agency;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class AgencyDataPersister implements ContextAwareDataPersisterInterface
{
    private EntityManagerInterface $_em;
    private UserInterface|null $_user;
    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->_em = $em;
        $this->_user = $security->getUser();
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Agency;
    }

    public function persist($data, array $context = [])
    {
        $this->_user->getCompany()->addAgency($data);
        $this->_em->persist($data);
        $this->_em->flush();
    }

    public function remove($data, array $context = [])
    {
        $this->_em->remove($data);
        $this->_em->flush();
    }
}