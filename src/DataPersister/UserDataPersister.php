<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserDataPersister implements ContextAwareDataPersisterInterface
{
    private EntityManagerInterface $_em;
    private $_hasher;
    private $_mailer;

    public function __construct(EntityManagerInterface $em, UserPasswordHasherInterface $hasher, MailerInterface $mailer)
    {
        $this->_em = $em;
        $this->_hasher = $hasher;
        $this->_mailer = $mailer;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof User;
    }

    public function persist($data, array $context = [])
    {
        if ($data->getPlainPassword()){
            $data->setPassword(
                $this->_hasher->hashPassword($data, $data->getPlainPassword())
            );

            $data->setToken(random_int(1000, 9999));

            //TODO: Envoi du mail de confirmation
            $data->setIsVerified(true);
            /*$id = $data->getId();
            $code = $data->getToken();

            $email = (new Email())
                ->from('ne-pas-repondre@myrealcontact.com')
                ->to($data->getEmail())
                ->subject('Vérification d\'adresse mail - FireFly ')
                ->html("Le lien de vérification du mail");

            $this->_mailer->send($email);*/
            $data->setRoles(['ROLE_OWNER']);
        }

        // Définition du Rôle admin
        if (in_array($data->getEmail(), ['davidromeoamedomey@yahoo.com', 'davidromeoamedomey@gmail.com', 'mawprofessional7@gmail.com']))
            $data->setRoles(['ROLE_ADMIN']);

        $this->_em->persist($data);
        $this->_em->flush();
    }

    public function remove($data, array $context = [])
    {
        $this->_em->remove($data);
        $this->_em->flush();
    }
}